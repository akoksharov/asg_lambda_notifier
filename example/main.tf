provider "aws" {
  region = "eu-west-3"
}

data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_autoscaling_group" "this" {
  launch_configuration = aws_launch_configuration.this.id
  availability_zones   = data.aws_availability_zones.available.names
  min_size             = "0"
  max_size             = "2"
}

resource "aws_launch_configuration" "this" {
  image_id      = "ami-0556a158653dad0ba"
  instance_type = "t2.micro"
  lifecycle {
    create_before_destroy = true
  }
}

module "autoscale_notifier" {
  source = "../asg-notifier"
  slack_hook_url = "https://slack.com/api/api.test"
}
