# ASG Notifier
lamda function to send notification to Slack for every event from AutoScalingGroup

## Notification example:
**AutoscalingGroupName:** tf-asg-20191127100457876700000005  
**Cause:** At 2019-11-27T10:23:04Z a user request update of AutoScalingGroup constraints to min: 0, max: 2, desired: 0 changing the desired capacity from 1 to 0.  At 2019-11-27T10:23:16Z an instance was taken out of service in response to a difference between desired and actual capacity, shrinking the capacity from 1 to 0.  At 2019-11-27T10:23:16Z instance i-00124aeb823d59412 was selected for termination.  
**StatusCode**: InProgress

## Install
Usage example is in ./example directory  
Prior to terraform apply command, build lambda executable by running ./build.sh