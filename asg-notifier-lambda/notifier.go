package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

type payloadBlockTextObject struct {
	Formating string `json:"type"`
	Text      string `json:"text"`
}

type messagePayloadBlock struct {
	BlockType string                 `json:"type"`
	Text      payloadBlockTextObject `json:"text"`
}

type messagePayload struct {
	Blocks []messagePayloadBlock `json:"blocks"`
}

func handleRequest(event events.AutoScalingEvent) error {
	log.Printf("event received:\n%v", event)

	msgText := fmt.Sprintf(
		"*AutoScalingGroupName:* %s\n*Cause:* %s\n*StatusCode:* %s",
		event.Detail["AutoScalingGroupName"].(string),
		event.Detail["Cause"].(string),
		event.Detail["StatusCode"].(string),
	)

	msgPayload := messagePayload{
		Blocks: []messagePayloadBlock{
			messagePayloadBlock{
				BlockType: "section",
				Text: payloadBlockTextObject{
					Formating: "mrkdwn",
					Text:      msgText,
				},
			},
		},
	}

	payloadBytes, err := json.Marshal(msgPayload)
	if err != nil {
		log.Printf("Cant marshal msg payload: %v", err)
		return err
	}
	log.Printf("%s", string(payloadBytes))
	payload := bytes.NewBuffer(payloadBytes)

	slackHookURL := os.Getenv("SLACK_HOOK_URL")

	httpClient := http.Client{}
	req, err := http.NewRequest("POST", slackHookURL, payload)
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		log.Printf("Cant create http request: %v", err)
		return err
	}
	rsp, err := httpClient.Do(req)
	if err != nil {
		log.Printf("http call failed: %v", err)
		return err
	}
	defer rsp.Body.Close()

	rspBody, _ := ioutil.ReadAll(rsp.Body)
	log.Printf("Notification delivery response: %s", rspBody)

	return nil
}

func main() {
	lambda.Start(handleRequest)
}
