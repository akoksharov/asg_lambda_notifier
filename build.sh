#!/bin/bash

pushd asg-notifier-lambda
GOOS=linux GOARCH=amd64 go build -o notifier
zip -j notifier.zip notifier
popd
