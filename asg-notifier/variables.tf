variable "autoscale_notifier_name" {
  description = "Name of autoscale notifier resources"
  default     = "autoscale_notifier"
}

variable "slack_hook_url" {
  description = "Slack hook URL"
  default     = "https://slack.com/api/api.test"
}
