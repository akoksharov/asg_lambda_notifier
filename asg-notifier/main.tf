
resource "aws_lambda_permission" "this" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.this.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.this.arn
}

resource "aws_cloudwatch_event_target" "this" {
  rule = aws_cloudwatch_event_rule.this.name
  arn  = aws_lambda_function.this.arn
}

resource "aws_cloudwatch_event_rule" "this" {
  name        = var.autoscale_notifier_name
  description = "Captures autoscale events"

  event_pattern = <<PATTERN
{
  "source": [
    "aws.autoscaling"
  ],
  "detail-type": [
    "EC2 Instance Launch Successful",
    "EC2 Instance Terminate Successful",
    "EC2 Instance Launch Unsuccessful",
    "EC2 Instance Terminate Unsuccessful"
  ]
}
PATTERN
}

resource "aws_lambda_function" "this" {
  function_name = var.autoscale_notifier_name
  filename      = "${path.module}/../asg-notifier-lambda/notifier.zip"
  handler       = "notifier"
  role          = aws_iam_role.this.arn
  runtime       = "go1.x"
  memory_size   = 128
  timeout       = 3

  environment {
    variables = {
      SLACK_HOOK_URL = var.slack_hook_url
    }
  }

  depends_on = [
    aws_cloudwatch_log_group.this,
  ]
}

resource "aws_cloudwatch_log_group" "this" {
  name              = "/aws/lambda/${var.autoscale_notifier_name}"
  retention_in_days = 1
}
